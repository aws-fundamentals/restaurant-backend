import { IsNotEmpty, IsString, Length } from 'class-validator';

export class CreateTableDto {
  @IsNotEmpty()
  seats: number;

  @IsNotEmpty()
  @Length(1)
  @IsString()
  status: string;
}
