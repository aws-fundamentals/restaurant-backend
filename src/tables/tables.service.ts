import { Injectable, NotFoundException, UseGuards } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { Repository } from 'typeorm';
import { CreateTableDto } from './dto/create-table.dto';
import { UpdateTableDto } from './dto/update-table.dto';
import { Table } from './entities/table.entity';

@UseGuards(JwtAuthGuard)
@Injectable()
export class TablesService {
  constructor(
    @InjectRepository(Table)
    private tablesRepository: Repository<Table>,
  ) {}
  create(createTableDto: CreateTableDto) {
    return this.tablesRepository.save(createTableDto);
  }

  findAll() {
    return this.tablesRepository.find({ relations: ['orders'] });
  }

  async findOne(id: number) {
    const table = await this.tablesRepository.findOne({
      where: { id: id },
      relations: ['orders'],
    });
    if (!table) {
      throw new NotFoundException();
    }
    return table;
  }

  async update(id: number, updateTableDto: UpdateTableDto) {
    const table = await this.tablesRepository.findOneBy({ id: id });
    if (!table) {
      throw new NotFoundException();
    }
    const updatedTable = { ...table, ...updateTableDto };
    return this.tablesRepository.save(updatedTable);
  }

  async remove(id: number) {
    // const table = await this.tablesRepository.findOneBy({ id: id });
    // if (!table) {
    //   throw new NotFoundException();
    // }
    return `Can't delete table ${id}`;
  }
}
