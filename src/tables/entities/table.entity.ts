import { Order } from 'src/orders/entities/order.entity';
import { Queue } from 'src/queues/entities/queue.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Table {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  seats: number;

  @Column({ length: 1 })
  status: string;

  @OneToMany(() => Order, (orders) => orders.table)
  orders: Order;

  @OneToMany(() => Queue, (queue) => queue.table)
  queue: Queue;
}
