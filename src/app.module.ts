import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Product } from './products/entities/product.entity';
import { ProductsModule } from './products/products.module';
import { OrdersModule } from './orders/orders.module';
import { TablesModule } from './tables/tables.module';
import { Order } from './orders/entities/order.entity';
import { Table } from './tables/entities/table.entity';
import { EmployeesModule } from './employees/employees.module';
import { Employee } from './employees/entities/employee.entity';
import { OrderItem } from './orders/entities/order-items.entity';
import { ReceiptsModule } from './receipts/receipts.module';
import { Receipt } from './receipts/entities/receipt.entity';
import { QueuesModule } from './queues/queues.module';
import { Queue } from './queues/entities/queue.entity';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'awsdatabase.chvuvwqi4o3e.ap-southeast-1.rds.amazonaws.com',
      port: 3306,
      username: 'admin',
      password: 'Pass1234',
      database: 'database',
      synchronize: true,
      logging: false,
      entities: [Product, Order, Table, Employee, OrderItem, Receipt, Queue],
      migrations: [],
      subscribers: [],
    }),
    ProductsModule,
    OrdersModule,
    TablesModule,
    EmployeesModule,
    ReceiptsModule,
    QueuesModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
