import { Order } from 'src/orders/entities/order.entity';
import { Queue } from 'src/queues/entities/queue.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  username: string;

  @Column()
  password: string;

  @Column()
  name: string;

  @Column()
  role: string;

  @Column({ length: 10 })
  phone: string;

  @Column()
  salary: number;

  @OneToMany(() => Order, (orders) => orders.employee)
  orders: Order;

  @OneToMany(() => Receipt, (receipt) => receipt.employee)
  receipt: Receipt;

  @OneToMany(() => Queue, (queue) => queue.employee)
  queue: Queue;
}
