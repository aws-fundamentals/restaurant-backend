import { IsNotEmpty, Length, Matches } from 'class-validator';

export class CreateEmployeeDto {
  @IsNotEmpty()
  username: string;

  @IsNotEmpty()
  @Matches(
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#])[A-Za-z\d@$!%*?&#]{8,}$/,
  )
  password: string;

  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  role: string;

  @IsNotEmpty()
  @Length(10)
  phone: string;

  @IsNotEmpty()
  salary: number;
}
