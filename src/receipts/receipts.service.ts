import { Injectable, UseGuards } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { Employee } from 'src/employees/entities/employee.entity';
import { Order } from 'src/orders/entities/order.entity';
import { Repository } from 'typeorm';
import { CreateReceiptDto } from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { Receipt } from './entities/receipt.entity';

@UseGuards(JwtAuthGuard)
@Injectable()
export class ReceiptsService {
  constructor(
    @InjectRepository(Receipt)
    private receiptsRepository: Repository<Receipt>,
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>,
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
  ) {}

  async create(createReceiptDto: CreateReceiptDto) {
    const receipt = new Receipt();
    receipt.total_price = createReceiptDto.total_price;
    receipt.discount = createReceiptDto.discount;
    receipt.cash = createReceiptDto.cash;
    receipt.change = createReceiptDto.change;
    receipt.employee = await this.employeesRepository.findOneBy({
      id: createReceiptDto.employeeId,
    });
    receipt.order = await this.ordersRepository.findOneBy({
      id: createReceiptDto.orderId,
    });
    return this.receiptsRepository.save(receipt);
  }

  findAll() {
    return this.receiptsRepository.find({
      relations: ['employee', 'order.orderItems.product', 'order.table'],
    });
  }

  findOne(id: number) {
    return this.receiptsRepository.findOneBy({ id });
  }

  async update(id: number, updateReceiptDto: UpdateReceiptDto) {
    await this.receiptsRepository.update(id, updateReceiptDto);
    return this.receiptsRepository.findOneBy({ id });
  }

  remove(id: number) {
    return this.receiptsRepository.softRemove({ id });
  }
}
