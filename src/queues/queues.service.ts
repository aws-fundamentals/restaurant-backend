import { Injectable, UseGuards, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { Repository } from 'typeorm';
import { CreateQueueDto } from './dto/create-queue.dto';
import { UpdateQueueDto } from './dto/update-queue.dto';
import { Queue } from './entities/queue.entity';
import { OrderItem } from 'src/orders/entities/order-items.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { Table } from 'src/tables/entities/table.entity';

@UseGuards(JwtAuthGuard)
@Injectable()
export class QueuesService {
  constructor(
    @InjectRepository(Queue)
    private queuesRepository: Repository<Queue>,
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
    @InjectRepository(Table)
    private tableRepository: Repository<Table>,
  ) {}

  async create(createQueueDto: CreateQueueDto) {
    const queue = new Queue();
    queue.name = createQueueDto.name;
    queue.status = createQueueDto.status;
    queue.table = await this.tableRepository.findOneBy({
      id: createQueueDto.tableId,
    });
    queue.orderItems = await this.orderItemsRepository.findOneBy({
      id: createQueueDto.orderItemsId,
    });
    return this.queuesRepository.save(queue);
  }

  findAll() {
    return this.queuesRepository.find({
      relations: ['employee', 'table', 'orderItems'],
    });
  }

  findOne(id: number) {
    return this.queuesRepository.findOne({
      where: { id: id },
      relations: ['employee', 'table', 'orderItems'],
    });
  }

  async update(id: number, updateQueueDto: UpdateQueueDto) {
    const queue = await this.queuesRepository.findOne({
      where: { id: id },
      relations: ['employee', 'table', 'orderItems'],
    });
    if (!queue) {
      throw new NotFoundException();
    }
    queue.status = updateQueueDto.status;
    queue.employee = await this.employeesRepository.findOneBy({
      id: updateQueueDto.employeeId,
    });
    return await this.queuesRepository.save(queue);
  }

  async remove(id: number) {
    return await this.queuesRepository.softRemove({ id });
  }
}
