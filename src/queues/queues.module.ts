import { Module } from '@nestjs/common';
import { QueuesService } from './queues.service';
import { QueuesController } from './queues.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Queue } from './entities/queue.entity';
import { Table } from 'src/tables/entities/table.entity';
import { OrderItem } from 'src/orders/entities/order-items.entity';
import { Employee } from 'src/employees/entities/employee.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Queue, Table, OrderItem, Employee])],
  controllers: [QueuesController],
  providers: [QueuesService],
})
export class QueuesModule {}
