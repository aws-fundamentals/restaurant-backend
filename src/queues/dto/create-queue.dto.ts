import { IsNotEmpty, IsString } from 'class-validator';

export class CreateQueueDto {
  @IsNotEmpty()
  tableId: number;

  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  status: string;

  @IsNotEmpty()
  orderItemsId: number;

  employeeId: number;
}
