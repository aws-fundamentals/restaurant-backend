import { IsNotEmpty, IsString, Length } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(4, 50)
  @IsString()
  name: string;

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  @IsString()
  img: string;
}
