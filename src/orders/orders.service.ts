import { Injectable, NotFoundException, UseGuards } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { Product } from 'src/products/entities/product.entity';
import { Table } from 'src/tables/entities/table.entity';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderItem } from './entities/order-items.entity';
import { Order } from './entities/order.entity';
import { Employee } from 'src/employees/entities/employee.entity';

@UseGuards(JwtAuthGuard)
@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(Table)
    private tablesRepository: Repository<Table>,
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>,
  ) {}

  async create(createOrderDto: CreateOrderDto) {
    const order: Order = new Order();
    order.table_number = createOrderDto.table_number;
    order.table = await this.tablesRepository.findOne({
      where: { id: createOrderDto.table_number },
    });
    order.employee = await this.employeesRepository.findOne({
      where: { id: createOrderDto.employeeId },
    });
    await this.ordersRepository.save(order);

    for (const od of createOrderDto.orderItems) {
      const orderItem = new OrderItem();
      orderItem.qty = od.qty;
      orderItem.product = await this.productsRepository.findOneBy({
        id: od.productId,
      });
      orderItem.name = orderItem.product.name;
      orderItem.orders = order;
      await this.orderItemsRepository.save(orderItem);
    }
    await this.ordersRepository.save(order);
    return this.ordersRepository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    });
  }

  findAll() {
    return this.ordersRepository.find({
      relations: ['orderItems.product', 'table', 'receipt'], // want to get product.price from orderItems for calculate total price
    });
  }

  async findOne(id: string) {
    const order = await this.ordersRepository.findOneBy({ id: id });
    if (!order) {
      throw new NotFoundException();
    }
    return this.ordersRepository.find({
      where: { id: id },
      relations: ['orderItems', 'orderItems.product', 'table', 'receipt'],
    });
  }

  async getOrderbyTableNumber(table_number: number): Promise<Order[]> {
    const order = await this.ordersRepository.find({
      where: { table_number: table_number },
      relations: ['orderItems', 'orderItems.product', 'table'],
    });
    if (!order) {
      throw new NotFoundException(
        `Order with table number ${table_number} not found`,
      );
    }
    return order;
  }

  async update(id: string, updateOrderDto: UpdateOrderDto) {
    const order = await this.ordersRepository.findOne({
      where: { id: id },
      relations: ['orderItems', 'orderItems.product', 'table'],
    });
    if (!order) {
      throw new NotFoundException();
    }
    if (!!updateOrderDto.table_number) {
      order.table_number = updateOrderDto.table_number;
      order.table.id = updateOrderDto.table_number;
      await this.ordersRepository.save(order);
    }
    if (!!updateOrderDto.orderItems) {
      for (const od of updateOrderDto.orderItems) {
        let check = true;
        const order_item = await this.orderItemsRepository.find({
          relations: ['orders'],
        });
        for (const item of order_item) {
          if (od.name === item.name && order.id === item.orders.id) {
            check = false;
            item.qty += od.qty;
            await this.orderItemsRepository.save(item);
            break;
          }
        }
        if (check) {
          const orderItem = new OrderItem();
          orderItem.qty = od.qty;
          orderItem.product = await this.productsRepository.findOneBy({
            name: od.name,
          });
          orderItem.name = orderItem.product.name;
          orderItem.orders = await this.ordersRepository.findOneBy({
            id: id,
          });
          await this.orderItemsRepository.save(orderItem);
        }
      }
    }
    return this.ordersRepository.findOne({
      where: { id: id },
      relations: ['orderItems'],
    });
  }

  async remove(id: string) {
    const order = await this.ordersRepository.findOneBy({ id: id });
    if (!order) {
      throw new NotFoundException();
    }
    return this.ordersRepository.softRemove(order);
  }

  async removeItem(id: number) {
    const item = await this.orderItemsRepository.findOneBy({ id: id });
    if (!item) {
      throw new NotFoundException();
    }
    if (item.qty === 1) {
      return this.orderItemsRepository.softRemove(item);
    } else {
      item.qty = item.qty - 1;
      return this.orderItemsRepository.save(item);
    }
  }
}
