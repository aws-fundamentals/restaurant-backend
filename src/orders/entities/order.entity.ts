import { Employee } from 'src/employees/entities/employee.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { Table } from 'src/tables/entities/table.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { OrderItem } from './order-items.entity';

@Entity()
export class Order {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  table_number: number;

  @ManyToOne(() => Table, (table) => table.orders)
  table: Table;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @ManyToOne(() => Employee, (employee) => employee.orders)
  employee: Employee;

  @OneToMany(() => OrderItem, (orderItems) => orderItems.orders)
  orderItems: OrderItem[];

  @OneToOne(() => Receipt, (receipt) => receipt.order)
  receipt: Receipt;
}
